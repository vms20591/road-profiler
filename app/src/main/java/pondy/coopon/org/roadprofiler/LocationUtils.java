package pondy.coopon.org.roadprofiler;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class LocationUtils {

    public static final int LOCATION_PERMISSION = 0;

                                                                        // Check Permission to Access for the Package
    public static boolean checkLocationPermission(Context context) {
        return (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

                                                                        // Request to Access Location Service, Data for this activity
    public static void requestLocationPermission(Activity targetActivity) {
        ActivityCompat.requestPermissions(targetActivity,
                new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION);
    }

}
