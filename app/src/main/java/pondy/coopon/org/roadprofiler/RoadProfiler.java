package pondy.coopon.org.roadprofiler;

import android.util.Log;

import java.util.ArrayList;

public class RoadProfiler {

    // Compute Average of Vertical Displacement
    public static float[] computeAverage(ArrayList<float[]> data) {
        float[] resultVector = new float[3];

        for (int i = 0; i < data.size(); i++)
            for (int j = 0; j < 3; j++)
                resultVector[j] += data.get(i)[j];

        for (int c = 0; c < 3; c++)
            resultVector[c] /= data.size();

        return resultVector;
    }

    // Acquire Vertical Acceleration with reference to Unit Vector from Accelerometer
    public static float computeVerticalAcceleration (float[] value, float[] refVector) {
        float valueHolder = 0;
        for (int i=0; i<3; i++)
            valueHolder += (value[i] * refVector[i]);

        float modOfRefVector = 0;
        for (int i=0; i<3; i++)
            modOfRefVector += (refVector[i] * refVector[i]);
        modOfRefVector = (float) Math.abs(Math.sqrt(modOfRefVector));

        return valueHolder/modOfRefVector;
    }

    /**
     * Compute International Roughness Index from the above Acquired Array of Values
     *
     *  REFERENCE: Assessing and Mapping of Road Surface Roughness
     *  based on GPS and Accelerometer Sensors on Bicycle-Mounted Smartphones.
     *
     * MDPI, Sensors, DOI: 10.3390/s18030914, 19 March 2018
     *
     * AUTHORS : Kaiyue Zang Jie Shen, Haosheng Huang, Mi Wan & Jiafeng Shi
     *
     *        SUM [Vertical Displacement over the complete Travel Duration]
     * IRI = -------------------------------------------------------------
     *                         Total Distance Travelled
     **/

    public static float computeIRI(ArrayList<Float> verticalAcceleration, int samplingInterval, float distanceTravelled) {
        ArrayList<Float> velocityFromAcceleration = new ArrayList<>();
        ArrayList<Float> displacementFromVelocity = new ArrayList<>();
        ArrayList<Float> verticalDisplacements = new ArrayList<>();


        /*
         Integrating the acceleration values over sampling time interval
         would give velocity over time.
         */
        for (int i=0; i<verticalAcceleration.size()-1; i++) {
            velocityFromAcceleration.add(trapezoidRule(verticalAcceleration.get(i), verticalAcceleration.get(i+1), samplingInterval));
        }

        /*
         Integrating the velocity values over sampling time interval
         would give displacement over time.
         */
        for (int i=0; i<velocityFromAcceleration.size()-1; i++) {
            displacementFromVelocity.add(trapezoidRule(velocityFromAcceleration.get(i), velocityFromAcceleration.get(i+1), samplingInterval));
        }

        /*
         not sure if this has to be done. collection of vertical displacements
         over time is represented as Vh in the paper. h(i) - h(i-1) is how it is defined.
         check Figure 2. and Equation (2). i.e the difference in longitudinal offset over the time.

         is longitudinal offset and vertical displacement same. If same, I think the below
         difference should be removed. Need clarification.
         */
        for (int i=1; i<displacementFromVelocity.size(); i++) {
            verticalDisplacements.add(displacementFromVelocity.get(i) - displacementFromVelocity.get(i-1));
        }

        float sumOfVerDisplacement = 0;
        for (float reading: verticalDisplacements)
            sumOfVerDisplacement += reading;

        return (sumOfVerDisplacement/distanceTravelled);
    }

    /**
     * Returns the result of applying TrapezoidRule for numerical integration.
     *
     * @param currentValue
     * @param nextValue
     * @param interval
     * @return
     */
    private static float trapezoidRule(float currentValue, float nextValue, int interval) {
        return (interval * ((currentValue + nextValue)/2));
    }
}
