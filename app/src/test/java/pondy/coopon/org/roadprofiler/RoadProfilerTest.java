package pondy.coopon.org.roadprofiler;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class RoadProfilerTest {

    @Test
    public void testComputerAverage() {
        ArrayList<float[]> sampleData = new ArrayList<>();
        for (int i=0; i<2; i++) {
            sampleData.add(new float[] {1,2,3});
        }

        float[] actual = RoadProfiler.computeAverage(sampleData);
        float[] expected = new float[] {1,2,3};

        Assert.assertArrayEquals(expected, actual, 1);

    }
}
